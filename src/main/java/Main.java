package main.java;

import main.java.person.ExpatPerson;
import main.java.person.LocalPerson;
import main.java.person.Person;

public class Main {

    public static void main(String[] args) {
        System.out.println("Hello World!");

        Person p1 = new Person();
        p1.setName("Test");
        p1.setAge(25);
        System.out.println(p1);


        LocalPerson lp = new LocalPerson();
        lp.setName("Local Person");
        lp.setAge(22);
        lp.setCiSeries("RX342343");

        ExpatPerson expatPerson = new ExpatPerson();
        expatPerson.setName("Expath Person");
        expatPerson.setAge(27);
        expatPerson.setPassport("1233243");
        System.out.println(expatPerson);

        Person nullPerson = null;
        try {
            System.out.println(nullPerson.getName());
        } catch (NullPointerException ex) {
            System.out.println("Ignoring NPE");
            ex.printStackTrace();
        }


    }
}
