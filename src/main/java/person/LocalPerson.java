package main.java.person;

public class LocalPerson extends Person {

    private String ciSeries;

    public String getCiSeries() {
        return ciSeries;
    }

    public void setCiSeries(String ciSeries) {
        this.ciSeries = ciSeries;
    }

    @Override
    public String toString() {
        return "LocalPerson{" +
                "name='" + getName() + '\'' +
                "age='" + getAge() + '\'' +
                "ciSeries='" + ciSeries + '\'' +
                '}';
    }
}
