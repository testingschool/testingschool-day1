package main.java.person;

public class ExpatPerson extends Person {

    private String passport;


    public String getPassport() {
        return passport;
    }

    public void setPassport(String passport) {
        this.passport = passport;
    }

    @Override
    public String toString() {
        return "ExpatPerson{" +
                "name='" + getName() + '\'' +
                "age='" + getAge() + '\'' +
                "passport='" + passport + '\'' +
                '}';
    }
}
